<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Currency;

class CurrenciesHistory extends Model
{
    use HasFactory;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'currencies_history';

    /* The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['currency_id', 'amount'];

    public function currencies()
    {
        return $this->hasOne(Currency::class, "id", "currency_id");
    }
}
