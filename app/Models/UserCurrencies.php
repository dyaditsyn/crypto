<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCurrencies extends Model
{
    use HasFactory;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_currencies';

    /* The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['user_id', 'currency_id'];

    protected $primaryKey = 'user_id';

    public $incrementing = false;
}
