<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\CurrenciesHistory;

class Currency extends Model
{
    use HasFactory, SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'currencies';

    /* The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['name', 'code'];

    public function currency()
    {
        return $this->hasMany(CurrenciesHistory::class, "currency_id", "id");
    }
}
