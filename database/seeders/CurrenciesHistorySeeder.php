<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CurrenciesHistory;

class CurrenciesHistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CurrenciesHistory::factory()->times(30)->create();
    }
}
