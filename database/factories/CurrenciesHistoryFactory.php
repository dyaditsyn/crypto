<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Currency;

class CurrenciesHistoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'amount' => $this->faker->randomFloat(2, 100, 200),
            'created_at' => $this->faker->dateTime(),
            'currency_id' => Currency::inRandomOrder()->value('id'),
        ];
    }
}
